# Kier SharePoint Development Style Guides

## General Rules

- No pushing to live without code review
- No modifications to live in work hours (9-5 UK Time) unless for a bug fix
- Create a folder (generally "App") in the site via webdav in order to make it inaccessible in the SharePoint interface
- Bundle scss and javascript into vendor and page specific bundles
- Minify all scripts and style resources for production
- Commit all changes to a git repo (further information below)
- Prefer csom where possible
- All actions must be permission trimmed
- All sites should work in all modern browsers
- All code must pass the style guide linters (in example project)

## Javascript

- Follow the style guide in javascript.md in this repo
- Use pnp to perform rest queries - https://github.com/pnp/pnp/blob/dev/packages/sp/docs/index.md
- NPM for package management
- Strongly prefer classes to target elements to allow for reusability

## CSS / SCSS

- Follow the style guide in css.md in this repo
- Prefer SCSS
- Use BEM for naming: http://getbem.com/introduction/
- No ids for styling
- Don't nest more that 3 levels deep

## Git

- All git repos to be provided by internal Kier employee
- Commits descriptions should be between 3 and 10 words long
- Commits should include a single change
- Master branch should always be production ready, if code is not then it should be in a test branch - preferably feature specific
- Commits should clearly describe the change, if that's not possible then it probably shouldn't be 1 commit