# C# Style Guide

All C# code should follow microsoft best practices defined in the documents below:

- [Coding Conventions](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions)
- [Secure Coding Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/security/secure-coding-guidelines)
- [Naming Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines)

## Naming

- Private fields should begin with an underscore and be camelcased. Avoid "this".

## Bot Framework

- Only use formflow for simple types, generally avoid for non-trivial cases